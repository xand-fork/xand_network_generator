pub(crate) mod adapters;
pub mod cli;
mod config;
pub(crate) mod contexts;
pub mod data;
pub(crate) mod entities;
pub(crate) mod error;
pub(crate) mod external_wrappers;
pub(crate) mod file_io;
pub(crate) mod key_provider;
pub mod network;
pub(crate) mod port;
pub(crate) mod xand_services;

use cli::{GenerateCmdArgs, XngCli};
use config::generate_config::GenerateConfig;
use contexts::docker_compose::{
    docker_compose_network::structured_docker_compose_root::StructuredDockerComposeRoot,
    DockerComposeNetwork,
};
pub use error::Result;
use external_wrappers::chain_spec_generation::ChainSpecBuilder;
use network::{GenerateMembersSpec, GenerateValidatorsSpec, XandEntitySet};
use std::env;

pub fn lib_main(cmd: XngCli) -> Result<()> {
    match cmd {
        XngCli::Generate { generate_config } => lib_generate(generate_config),
    }
}

pub(crate) fn lib_generate(args: GenerateCmdArgs) -> Result<()> {
    // Initialize directory used
    let cwd = env::current_dir().unwrap();
    let config = GenerateConfig::load(&args.config_file)?;

    let work_dir = match args.output_dir.is_absolute() {
        true => args.output_dir,
        false => cwd.join(args.output_dir),
    };
    let dc_dir = StructuredDockerComposeRoot::build(work_dir)?;

    // Generate constant number of extra members and validators.
    let members_spec = GenerateMembersSpec::new(config.num_members, config.num_extra_members);
    let validators_spec =
        GenerateValidatorsSpec::new(config.num_validators, config.num_extra_validators);

    let enabled_banks = config.get_enabled_banks_or_default();

    // Generate XandEntitySet
    let entity_set = XandEntitySet::generate(
        members_spec,
        validators_spec,
        config.key_generation_seed,
        enabled_banks,
        config.enable_auth,
    )?;

    // Generate chain spec from entity set
    let chainspec_zip = match args.chainspec_zip.is_absolute() {
        true => args.chainspec_zip,
        false => cwd.join(args.chainspec_zip),
    };
    let builder = ChainSpecBuilder::new(
        chainspec_zip,
        config.minor_units_per_validator_emission,
        config.block_quota,
    );
    let chain_spec = builder.build(&entity_set, dc_dir.chain_spec_dir()).unwrap();

    // Create Docker Context
    let docker_context = DockerComposeNetwork::new(entity_set, config.images, chain_spec);

    // Build each entity's dir structure
    docker_context.serialize_entities(&dc_dir)?;
    docker_context.write_docker_compose_yaml(&dc_dir)?;

    //Write entities' metadata to file for downstream puppeteer to consume
    docker_context.summarize_entities(&dc_dir)?;

    Ok(())
}
