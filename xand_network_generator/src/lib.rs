#![forbid(unsafe_code)]
#![allow(non_snake_case)]

#[cfg(test)]
#[macro_use]
extern crate matches;

#[macro_use]
extern crate serde_derive;

pub mod app;

pub use app::error::{Error, Result};

pub mod contracts;
