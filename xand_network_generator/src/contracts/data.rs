use xand_address::Address;

pub struct MemberAddress(pub(crate) Address);

pub struct ValidatorAddress(pub(crate) Address);

pub struct TrustAddress(pub(crate) Address);

pub enum NetworkParticipant {
    Member,
    Validator,
    Trust,
    LimitedAgent,
}

pub mod bank_info {

    pub const PROVIDENT_ROUTING: u32 = 211374020;
    pub const PROVIDENT_TRUST_ACCT: u32 = 1000000;

    pub const MCB_ROUTING: u32 = 121141343;
    pub const MCB_TRUST_ACCT: u32 = 1000000;

    pub const MCB_FAILING_ROUTING: u32 = 101010101;
    pub const MCB_FAILING_TRUST_ACCT: u32 = 1000000;

    // dummy routing numbers -- invalid in practice
    pub const TEST_BANK_ONE_ROUTING: u32 = 990000000;
    pub const TEST_BANK_ONE_TRUST_ACCT: u32 = 1000000;

    pub const TEST_BANK_TWO_ROUTING: u32 = 991000000;
    pub const TEST_BANK_TWO_TRUST_ACCT: u32 = 1000000;

    pub const MEMBER_MCB_INITIAL_ACCOUNT: u32 = 2000000;
    pub const MEMBER_PROVIDENT_INITIAL_ACCOUNT: u32 = 3000000;
    pub const MEMBER_MCB_FAILING_INITIAL_ACCOUNT: u32 = 4000000;
    pub const MEMBER_TEST_BANK_ONE_INITIAL_ACCOUNT: u32 = 8000000;
    pub const MEMBER_TEST_BANK_TWO_INITIAL_ACCOUNT: u32 = 9000000;

    #[derive(
        Debug,
        Clone,
        PartialEq,
        Copy,
        Eq,
        Hash,
        Serialize,
        Deserialize,
        strum::ToString,
        strum::EnumIter,
    )]
    #[serde(rename_all = "kebab-case")]
    pub enum Bank {
        Mcb,
        Provident,

        // dummy banks for Xandbox
        TestBankOne,
        TestBankTwo,
    }

    /// Defines an ordering for Bank variants, sorted alphabetically by name.
    impl Ord for Bank {
        fn cmp(&self, other: &Self) -> std::cmp::Ordering {
            self.to_string().cmp(&other.to_string())
        }
    }

    impl PartialOrd for Bank {
        fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
            Some(self.cmp(other))
        }
    }

    impl Bank {
        pub fn get_url_name(&self) -> String {
            match self {
                Bank::Mcb => "metropolitan".into(),
                Bank::Provident => "provident".into(),
                Bank::TestBankOne => "test_bank_one".into(),
                Bank::TestBankTwo => "test_bank_two".into(),
            }
        }

        pub fn get_routing_number(&self) -> u32 {
            match self {
                Bank::Mcb => MCB_ROUTING,
                Bank::Provident => PROVIDENT_ROUTING,
                Bank::TestBankOne => TEST_BANK_ONE_ROUTING,
                Bank::TestBankTwo => TEST_BANK_TWO_ROUTING,
            }
        }

        pub fn get_member_account_range_start(&self) -> u32 {
            match self {
                Bank::Mcb => MEMBER_MCB_INITIAL_ACCOUNT,
                Bank::Provident => MEMBER_PROVIDENT_INITIAL_ACCOUNT,
                Bank::TestBankOne => MEMBER_TEST_BANK_ONE_INITIAL_ACCOUNT,
                Bank::TestBankTwo => MEMBER_TEST_BANK_TWO_INITIAL_ACCOUNT,
            }
        }

        pub fn get_trust_account_number(&self) -> u32 {
            match self {
                Bank::Mcb => MCB_TRUST_ACCT,
                Bank::Provident => PROVIDENT_TRUST_ACCT,
                Bank::TestBankOne => TEST_BANK_ONE_TRUST_ACCT,
                Bank::TestBankTwo => TEST_BANK_TWO_TRUST_ACCT,
            }
        }

        pub fn get_trust_account(&self) -> Account {
            Account::new(self.get_routing_number(), self.get_trust_account_number())
        }

        pub fn get_trust_account_bank_info(&self) -> BankInfo {
            BankInfo::new(*self, self.get_trust_account())
        }
    }

    #[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
    pub struct Account {
        routing_number: u32,
        account_number: u32,
    }

    impl Account {
        pub fn new(routing_number: u32, account_number: u32) -> Self {
            Self {
                routing_number,
                account_number,
            }
        }

        pub fn account_number(&self) -> u32 {
            self.account_number
        }
        pub fn routing_number(&self) -> u32 {
            self.routing_number
        }
    }

    #[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
    pub struct BankInfo {
        pub bank: Bank,
        pub account: Account,
    }

    impl BankInfo {
        pub fn new(bank: Bank, account: Account) -> Self {
            Self { bank, account }
        }
        pub fn routing_number(&self) -> u32 {
            self.account.routing_number
        }

        pub fn account_number(&self) -> u32 {
            self.account.account_number
        }
    }
}
