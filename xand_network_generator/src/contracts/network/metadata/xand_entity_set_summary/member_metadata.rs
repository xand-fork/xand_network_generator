use super::{MemberApiDetails, XandApiDetails};
use crate::contracts::data::bank_info::BankInfo;

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct MemberMetadata {
    pub entity_name: String,
    pub address: String,

    pub xand_api_details: XandApiDetails,
    pub member_api_details: MemberApiDetails,

    pub banks: Vec<BankInfo>,
    pub encryption_public_key: String,

    pub initially_permissioned: bool,
}
