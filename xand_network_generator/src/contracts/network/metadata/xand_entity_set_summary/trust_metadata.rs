use super::XandApiDetails;
use crate::contracts::data::bank_info::BankInfo;
use url::Url;

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct TrustMetadata {
    pub address: String,
    pub xand_api_details: XandApiDetails,
    pub banks: Vec<BankInfo>,
    pub encryption_public_key: String,
    pub trust_api_url: Url,
}
