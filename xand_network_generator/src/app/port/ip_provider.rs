use std::net::Ipv4Addr;

/// Types impl'ing this trait will be used for generating dynamic network definitions
pub trait IpProvider {
    fn next(&mut self) -> Ipv4Addr;
}
