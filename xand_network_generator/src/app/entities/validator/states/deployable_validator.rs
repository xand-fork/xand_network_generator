use crate::{
    app::{
        entities::validator::states::networked_validator::NetworkedValidator,
        external_wrappers::DomainXandKeyPair,
    },
    contracts::{data::bank_info::BankInfo, network::auth::JwtInfo},
};
use tpfs_krypt::KeyPairInstance;

#[derive(Clone, Debug)]
pub struct DeployableValidator {
    networked_validator: NetworkedValidator,
    validator_bank_info: Vec<BankInfo>,
    xand_api_auth: Option<JwtInfo>,
}

impl DeployableValidator {
    pub fn new(
        networked_validator: NetworkedValidator,
        validator_bank_info: Vec<BankInfo>,
        xand_api_auth: Option<JwtInfo>,
    ) -> Self {
        Self {
            networked_validator,
            validator_bank_info,
            xand_api_auth,
        }
    }

    /// Deployable Validator getters
    pub(crate) fn networked_validator(&self) -> &NetworkedValidator {
        &self.networked_validator
    }
    pub(crate) fn validator_bank_info(&self) -> &Vec<BankInfo> {
        &self.validator_bank_info
    }
    pub(crate) fn xand_api_auth(&self) -> &Option<JwtInfo> {
        &self.xand_api_auth
    }

    /// Address getters
    pub(crate) fn node_key(&self) -> String {
        self.networked_validator().identity().node_key()
    }
    pub(crate) fn validator_address(&self) -> String {
        self.validator_authority_kp().get_address()
    }
    pub(crate) fn validator_block_production_address(&self) -> String {
        self.validator_block_production_kp().get_address()
    }
    pub(crate) fn validator_block_finalization_address(&self) -> String {
        self.validator_block_finalization_kp().get_address()
    }

    /// Xand keypair getters
    pub(crate) fn validator_block_production_kp(&self) -> &DomainXandKeyPair {
        self.networked_validator.identity().produce_blocks_kp()
    }
    pub(crate) fn validator_block_finalization_kp(&self) -> &DomainXandKeyPair {
        self.networked_validator.identity().finalize_blocks_kp()
    }
    pub(crate) fn validator_libp2p_kp(&self) -> &DomainXandKeyPair {
        self.networked_validator.identity().libp2p_kp()
    }
    pub(crate) fn validator_authority_kp(&self) -> &DomainXandKeyPair {
        self.networked_validator.identity().authority_kp()
    }
    pub(crate) fn validator_encryption_kp(&self) -> &KeyPairInstance {
        self.networked_validator.identity().encryption_kp()
    }
}
