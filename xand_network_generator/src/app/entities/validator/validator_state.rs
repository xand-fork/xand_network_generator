#[derive(Clone)]
pub enum ValidatorState<S> {
    Walleted(S),
    Networked(S),
    Deployable(S),
}

impl<S: Clone> ValidatorState<S> {
    pub fn take(self) -> S {
        match self {
            ValidatorState::Walleted(s) => s,
            ValidatorState::Networked(s) => s,
            ValidatorState::Deployable(s) => s,
        }
    }

    pub fn borrow(&self) -> &S {
        match self {
            ValidatorState::Walleted(s) => s,
            ValidatorState::Networked(s) => s,
            ValidatorState::Deployable(s) => s,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fmt::{Debug, Formatter};

    impl<S: Clone> Debug for ValidatorState<S> {
        fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
            let state_str = match self {
                ValidatorState::Walleted(_) => "Walleted",
                ValidatorState::Networked(_) => "Networked",
                ValidatorState::Deployable(_) => "Deployable",
            };
            write!(f, "{}", state_str)
        }
    }
}
