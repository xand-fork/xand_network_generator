use crate::{
    app::{entities::trust::data::TrustBankAccountSet, external_wrappers::DomainXandKeyPair},
    contracts::network::auth::JwtInfo,
};
use std::net::Ipv4Addr;
use tpfs_krypt::KeyPairInstance;

#[derive(Clone)]
pub struct DeployableTrust {
    pub kp: DomainXandKeyPair,
    pub ips: Vec<Ipv4Addr>,
    pub acct_set: TrustBankAccountSet,
    pub encryption_key_pair: KeyPairInstance,
    pub xand_api_auth: Option<JwtInfo>,
}

impl DeployableTrust {
    pub fn new(
        kp: DomainXandKeyPair,
        ips: Vec<Ipv4Addr>,
        acct_set: TrustBankAccountSet,
        encryption_key_pair: KeyPairInstance,
        xand_api_auth: Option<JwtInfo>,
    ) -> Self {
        DeployableTrust {
            kp,
            ips,
            acct_set,
            encryption_key_pair,
            xand_api_auth,
        }
    }
}
