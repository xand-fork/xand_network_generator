use crate::{
    app::{external_wrappers::DomainXandKeyPair, key_provider::MemberKeySet},
    contracts::data::MemberAddress,
};
use std::convert::TryInto;
use tpfs_krypt::KeyPairInstance;

#[derive(Clone, Debug)]
pub struct WalletedMember {
    kp: DomainXandKeyPair,
    encryption_key_pair: KeyPairInstance,
}

impl WalletedMember {
    pub fn new(keys: MemberKeySet) -> Self {
        WalletedMember {
            kp: keys.key,
            encryption_key_pair: keys.encryption_key,
        }
    }

    pub fn get_address(&self) -> MemberAddress {
        MemberAddress(self.kp.get_address().try_into().unwrap())
    }

    pub fn kp(&self) -> &DomainXandKeyPair {
        &self.kp
    }

    pub fn encryption_key_pair(&self) -> &KeyPairInstance {
        &self.encryption_key_pair
    }
}
