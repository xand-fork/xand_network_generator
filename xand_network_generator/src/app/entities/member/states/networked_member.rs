use crate::app::entities::member::states::walleted_member::WalletedMember;
use std::net::Ipv4Addr;

#[derive(Clone, Debug)]
pub struct NetworkedMember {
    pub identity: WalletedMember,
    pub ip_addrs: Vec<Ipv4Addr>,
}

impl NetworkedMember {
    pub fn new(identity: WalletedMember, ip_addrs: Vec<Ipv4Addr>) -> Self {
        Self { identity, ip_addrs }
    }
}
