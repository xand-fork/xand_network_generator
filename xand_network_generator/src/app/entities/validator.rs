use crate::{
    app::{
        entities::validator::{
            states::{
                deployable_validator::DeployableValidator, networked_validator::NetworkedValidator,
                walleted_validator::WalletedValidator,
            },
            validator_state::ValidatorState,
        },
        external_wrappers::DomainXandKeyPair,
        key_provider::ValidatorKeySet,
    },
    contracts::{data::bank_info::BankInfo, network::auth::JwtInfo},
};
use std::net::Ipv4Addr;
use tpfs_krypt::{KeyPair, KeyPairInstance, PublicKey};

pub mod states;
mod validator_state;

#[cfg(test)]
mod tests;

#[derive(Clone)]
pub struct Validator<S> {
    state: ValidatorState<S>,
}

impl<S> Validator<S> {
    pub fn state(&self) -> &ValidatorState<S> {
        &self.state
    }
}
impl Validator<WalletedValidator> {
    pub fn new(keys: ValidatorKeySet) -> Self {
        let id = WalletedValidator::new(keys);
        Validator {
            state: ValidatorState::Walleted(id),
        }
    }
    /// State transition function consuming `Validator<WalletedValidator>`
    /// and producing `Validator<NetworkedValidator>`
    pub fn transition(self, ip_addrs: Vec<Ipv4Addr>) -> Validator<NetworkedValidator> {
        let networked_validator = NetworkedValidator::new(self.state.take(), ip_addrs);
        Validator {
            state: ValidatorState::Networked(networked_validator),
        }
    }
}

impl Validator<NetworkedValidator> {
    // State transition function
    pub fn transition<T: IntoIterator<Item = BankInfo>>(
        self,
        bank_info: T,
        xand_api_auth: Option<JwtInfo>,
    ) -> Validator<DeployableValidator> {
        let deployable_validator = DeployableValidator::new(
            self.state.take(),
            bank_info.into_iter().collect(),
            xand_api_auth,
        );
        Validator {
            state: ValidatorState::Deployable(deployable_validator),
        }
    }
}

impl Validator<DeployableValidator> {
    pub fn libp2p_kp(&self) -> &DomainXandKeyPair {
        self.state.borrow().validator_libp2p_kp()
    }

    pub fn bank_info(&self) -> &Vec<BankInfo> {
        self.state.borrow().validator_bank_info()
    }

    pub fn ips(&self) -> &Vec<Ipv4Addr> {
        self.state.borrow().networked_validator().ips()
    }

    pub fn encryption_key(&self) -> PublicKey {
        self.encryption_key_pair()
            .public()
            .try_into()
            .expect("Public part of key pair must fit in PublicKey")
    }

    pub fn encryption_key_pair(&self) -> &KeyPairInstance {
        self.state
            .borrow()
            .networked_validator()
            .identity()
            .encryption_kp()
    }
}
