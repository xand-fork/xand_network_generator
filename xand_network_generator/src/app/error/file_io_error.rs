use std::{io, path::PathBuf};
use thiserror::Error;

#[derive(Debug, Error)]
pub enum FileIoError {
    #[error("While creating directory {path:?}: {err}")]
    CreateDir {
        path: PathBuf,
        err: fs_extra::error::Error,
    },
    #[error("While creating file {path:?}: {err}")]
    CreateFile { path: PathBuf, err: io::Error },
    #[error("While writing file {path:?}: {err}")]
    WriteFile { path: PathBuf, err: io::Error },
    #[error("While copying file {source_path:?} to {dest_path:?}: {err}")]
    CopyFile {
        source_path: PathBuf,
        dest_path: PathBuf,
        err: io::Error,
    },
    #[error("While reading file {path:?}: {err}")]
    ReadFile { path: PathBuf, err: io::Error },
    #[error("While opening file {path:?}: {err}")]
    OpenFile { path: PathBuf, err: io::Error },
}

#[cfg(test)]
mod tests {
    use super::FileIoError;

    #[test]
    fn create_file_error__display_formats_correctly() {
        let error = FileIoError::CreateFile {
            path: "some/path/".into(),
            err: std::io::Error::new(std::io::ErrorKind::NotFound, "something bad happened"),
        };

        let error_string = error.to_string();

        assert_eq!(
            error_string,
            "While creating file \"some/path/\": something bad happened"
        );
    }

    #[test]
    fn copy_file_error__display_formats_correctly() {
        let error = FileIoError::CopyFile {
            source_path: "some/file.txt".into(),
            dest_path: "another/file.txt".into(),
            err: std::io::Error::new(std::io::ErrorKind::NotFound, "something bad happened"),
        };

        let error_string = error.to_string();

        assert_eq!(
            error_string,
            "While copying file \"some/file.txt\" to \"another/file.txt\": something bad happened"
        );
    }
}
