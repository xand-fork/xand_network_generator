use tpfs_krypt::KeyPairInstance;

use crate::app::{external_wrappers::DomainXandKeyPair, key_provider::KryptRng};

#[derive(Clone, Debug)]
pub struct MemberKeySet {
    pub key: DomainXandKeyPair,
    pub encryption_key: KeyPairInstance,
}

impl MemberKeySet {
    pub(crate) fn generate_with<R: KryptRng>(rng: &mut R) -> Self {
        Self {
            key: DomainXandKeyPair::generate_member_key(rng),
            encryption_key: KeyPairInstance::generate_with(
                tpfs_krypt::KeyType::SharedEncryptionX25519,
                rng,
            )
            .expect("Key pair generation should not fail"),
        }
    }
}

#[cfg(test)]
mod tests {
    use rand::SeedableRng;

    use super::MemberKeySet;
    use tpfs_krypt::KeyPair;

    #[test]
    fn generate_with__generates_unique_keys_for_different_random_inputs() {
        let mut rng1 = rand::rngs::StdRng::seed_from_u64(0);
        let mut rng2 = rand::rngs::StdRng::seed_from_u64(1);

        let keys1 = MemberKeySet::generate_with(&mut rng1);
        let keys2 = MemberKeySet::generate_with(&mut rng2);

        assert_ne!(keys1.key.get_address(), keys2.key.get_address());
        assert_ne!(
            keys1.encryption_key.identifier().value,
            keys2.encryption_key.identifier().value
        );
    }

    #[test]
    fn generate_with__generates_reproducible_keys_for_same_seed() {
        let mut rng1 = rand::rngs::StdRng::seed_from_u64(0);
        let mut rng2 = rand::rngs::StdRng::seed_from_u64(0);

        let keys1 = MemberKeySet::generate_with(&mut rng1);
        let keys2 = MemberKeySet::generate_with(&mut rng2);

        assert_eq!(keys1, keys2);
    }

    impl PartialEq for MemberKeySet {
        fn eq(&self, other: &Self) -> bool {
            self.key.get_address() == other.key.get_address()
                && self.encryption_key.identifier().value == other.encryption_key.identifier().value
        }
    }
}
