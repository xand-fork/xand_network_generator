use crate::{
    app::adapters::auth_provider::{error::AuthProviderError, jwt_provider::JwtProvider},
    contracts::network::auth::{Jwt, JwtInfo, JwtSecret},
};

pub mod error;
mod jwt_provider;

/// JWT secret Member API will use to authorize tokens
pub const MEMBER_API_JWT_SECRET: &str = "InsecureSecretKeyForMemberApi";
/// JWT secret Xand API will use to authorize tokens
pub const XAND_API_JWT_SECRET: &str = "InsecureSecretKeyForXandAPI";

/// For mock networks, provides secrets and jwt tokens for entities to incorporate into
/// their services
#[derive(Default)]
pub struct AuthProvider {}

impl AuthProvider {
    pub fn member_api_jwt_info(&self) -> Result<JwtInfo, AuthProviderError> {
        let secret = JwtSecret(MEMBER_API_JWT_SECRET.to_string());
        let token: Jwt = JwtProvider::try_create(&secret)?;
        let jwt_info = JwtInfo { secret, token };
        Ok(jwt_info)
    }

    pub fn xand_api_jwt_info(&self) -> Result<JwtInfo, AuthProviderError> {
        let secret = JwtSecret(XAND_API_JWT_SECRET.to_string());
        let token: Jwt = JwtProvider::try_create(&secret)?;
        let jwt_info = JwtInfo { secret, token };
        Ok(jwt_info)
    }

    /// Utility method that will infallibly return a JWT for testing
    #[cfg(test)]
    pub fn get_test_xand_api_jwt_info() -> JwtInfo {
        AuthProvider::default().xand_api_jwt_info().unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn member_api_jwt_info__returns_non_empty_token_from_const_secret() {
        // Given
        let auth = AuthProvider {};

        // When
        let jwt_info = auth.member_api_jwt_info().unwrap();

        // Then
        assert_eq!(jwt_info.secret.0, MEMBER_API_JWT_SECRET);
        assert!(!jwt_info.token.0.is_empty())
    }

    #[test]
    fn xand_api_jwt_info__returns_non_empty_token_from_const_secret() {
        // Given
        let auth = AuthProvider {};

        // When
        let jwt_info = auth.xand_api_jwt_info().unwrap();

        // Then
        assert_eq!(jwt_info.secret.0, XAND_API_JWT_SECRET);
        assert!(!jwt_info.token.0.is_empty())
    }
}
