use jwtcli::jwt::error::Error;
use thiserror::Error;

pub type Result<T, E = JwtProviderError> = std::result::Result<T, E>;

#[derive(Clone, Debug, Error, PartialEq, Eq)]
pub enum JwtProviderError {
    #[error("{0}")]
    JwtCliError(String),
}

impl From<::jwtcli::jwt::error::Error> for JwtProviderError {
    fn from(e: Error) -> Self {
        JwtProviderError::JwtCliError(e.to_string())
    }
}
