use std::collections::HashMap;

use crate::{
    app::port::bank_provider::BankProvider,
    contracts::data::bank_info::{Account, Bank, BankInfo},
};

pub struct DockerBankInfoProvider {
    bank_idx: HashMap<Bank, u32>,
}

impl DockerBankInfoProvider {
    pub fn new() -> Self {
        Self {
            bank_idx: HashMap::new(),
        }
    }

    pub fn next(&mut self, bank: Bank) -> BankInfo {
        let this_acct = *self.bank_idx.get(&bank).unwrap_or(&0);
        self.bank_idx.insert(bank, this_acct + 1);

        let routing_num = bank.get_routing_number();
        let initial_account = bank.get_member_account_range_start();
        let account_num = initial_account + this_acct;

        BankInfo::new(bank, Account::new(routing_num, account_num))
    }
}

impl BankProvider for DockerBankInfoProvider {
    fn get_trust(&self, bank: Bank) -> BankInfo {
        bank.get_trust_account_bank_info()
    }
}
#[cfg(test)]
mod tests {
    use crate::{
        app::{
            adapters::bank_info_provider::docker_bank_info_provider::DockerBankInfoProvider,
            port::bank_provider::BankProvider,
        },
        contracts::data::bank_info::{
            Account, Bank, BankInfo, MCB_ROUTING, MCB_TRUST_ACCT, MEMBER_MCB_INITIAL_ACCOUNT,
            MEMBER_PROVIDENT_INITIAL_ACCOUNT, PROVIDENT_ROUTING, PROVIDENT_TRUST_ACCT,
        },
    };

    #[test]
    fn next__returns_valid_mcb_account() {
        // Given
        let mut context = DockerBankInfoProvider::new();

        // When
        let bank_info = context.next(Bank::Mcb);

        // Then
        let expected = BankInfo::new(
            Bank::Mcb,
            Account::new(MCB_ROUTING, MEMBER_MCB_INITIAL_ACCOUNT),
        );
        assert_eq!(bank_info, expected)
    }

    #[test]
    fn next__returns_valid_provident_account() {
        // Given
        let mut context = DockerBankInfoProvider::new();

        // When
        let bank_info = context.next(Bank::Provident);

        // Then
        let expected = BankInfo::new(
            Bank::Provident,
            Account::new(PROVIDENT_ROUTING, MEMBER_PROVIDENT_INITIAL_ACCOUNT),
        );
        assert_eq!(bank_info, expected)
    }

    #[test]
    fn next__2nd_invocation_returns_incremented_account() {
        // Given
        let mut context = DockerBankInfoProvider::new();

        // When
        let _first = context.next(Bank::Mcb);
        let bank_info = context.next(Bank::Mcb);

        // Then
        let expected = BankInfo::new(
            Bank::Mcb,
            Account::new(MCB_ROUTING, MEMBER_MCB_INITIAL_ACCOUNT + 1),
        );
        assert_eq!(bank_info, expected)
    }

    #[test]
    fn next__alternating_increments_banks_separately() {
        // Given
        let mut context = DockerBankInfoProvider::new();

        // When
        let bank_info_0 = context.next(Bank::Provident);
        let bank_info_1 = context.next(Bank::Mcb);
        let bank_info_2 = context.next(Bank::Provident);
        let bank_info_3 = context.next(Bank::Mcb);

        // Then
        assert_eq!(
            bank_info_0.account.account_number(),
            MEMBER_PROVIDENT_INITIAL_ACCOUNT
        );
        assert_eq!(
            bank_info_1.account.account_number(),
            MEMBER_MCB_INITIAL_ACCOUNT
        );
        assert_eq!(
            bank_info_2.account.account_number(),
            MEMBER_PROVIDENT_INITIAL_ACCOUNT + 1
        );
        assert_eq!(
            bank_info_3.account.account_number(),
            MEMBER_MCB_INITIAL_ACCOUNT + 1
        );
    }

    #[test]
    fn get_trust__returns_constant_mcb_account() {
        // Given
        let expected_account = BankInfo::new(Bank::Mcb, Account::new(MCB_ROUTING, MCB_TRUST_ACCT));
        let context = DockerBankInfoProvider::new();

        // When
        let res = context.get_trust(Bank::Mcb);
        let res_2 = context.get_trust(Bank::Mcb);

        // Then
        assert_eq!(res, expected_account);
        assert_eq!(res_2, expected_account);
    }

    #[test]
    fn get_trust__returns_constant_prov_account() {
        // Given
        let expected_account = BankInfo::new(
            Bank::Provident,
            Account::new(PROVIDENT_ROUTING, PROVIDENT_TRUST_ACCT),
        );
        let context = DockerBankInfoProvider::new();

        // When
        let res = context.get_trust(Bank::Provident);
        let res_2 = context.get_trust(Bank::Provident);

        // Then
        assert_eq!(res, expected_account);
        assert_eq!(res_2, expected_account);
    }
}
