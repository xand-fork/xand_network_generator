use crate::app::port::ip_provider::IpProvider;
use ipnet::{Ipv4AddrRange, Ipv4Net};
use std::net::Ipv4Addr;

pub struct DockerIpProvider {
    #[allow(dead_code)]
    subnet: Ipv4Net,
    ip_range: Ipv4AddrRange,
}

impl DockerIpProvider {
    #[cfg(test)]
    pub fn test() -> Self {
        let subnet: Ipv4Net = "172.28.0.0/16".parse().unwrap();
        DockerIpProvider::new(subnet)
    }

    pub fn new(subnet: Ipv4Net) -> Self {
        Self {
            subnet,
            ip_range: subnet.hosts(),
        }
    }

    #[cfg(test)]
    pub fn get_subnet(&self) -> Ipv4Net {
        self.subnet
    }
}

impl IpProvider for DockerIpProvider {
    fn next(&mut self) -> Ipv4Addr {
        self.ip_range.next().unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::{net::Ipv4Addr, str::FromStr};

    #[test]
    fn new__can_initialize_with_subnet() {
        let subnet: Ipv4Net = "172.28.0.0/16".parse().unwrap();
        let _ip_provider = DockerIpProvider::new(subnet);
    }

    #[test]
    fn get_subnet__returns_subnet_value_from_initialization() {
        // Given
        let subnet: Ipv4Net = "172.28.0.0/16".parse().unwrap();
        let ip_provider = DockerIpProvider::new(subnet);

        // When
        let res = ip_provider.get_subnet().to_string();

        // Then
        assert_eq!(res, "172.28.0.0/16");
    }

    #[test]
    fn iter_ips__returns_valid_ips() {
        // Given
        let subnet: Ipv4Net = "172.28.0.0/16".parse().unwrap();
        let mut ip_provider = DockerIpProvider::new(subnet);

        // When
        let res: Vec<Ipv4Addr> = vec![ip_provider.next()];

        // Then
        let valid_ip = Ipv4Addr::from_str("172.28.0.1").unwrap();
        let network_ip = Ipv4Addr::from_str("172.28.0.0").unwrap();
        let broadcast_ip = Ipv4Addr::from_str("172.28.255.255").unwrap();

        assert!(res.contains(&valid_ip));
        assert!(!res.contains(&network_ip));
        assert!(!res.contains(&broadcast_ip));
    }
}
