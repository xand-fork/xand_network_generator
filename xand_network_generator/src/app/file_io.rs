use crate::{app::error::file_io_error::FileIoError, Result};
use std::{fs::File, path::Path};

pub fn create_dir<P: AsRef<Path>>(path: P, erase: bool) -> Result<(), FileIoError> {
    fs_extra::dir::create(&path, erase).map_err(|e| FileIoError::CreateDir {
        path: path.as_ref().into(),
        err: e,
    })
}

pub fn write_file<P, B>(path: P, contents: B) -> Result<(), FileIoError>
where
    P: AsRef<Path>,
    B: AsRef<[u8]>,
{
    std::fs::write(path.as_ref(), contents).map_err(|e| FileIoError::WriteFile {
        path: path.as_ref().into(),
        err: e,
    })
}

pub fn copy_file<S, D>(source: S, dest: D) -> Result<(), FileIoError>
where
    S: AsRef<Path>,
    D: AsRef<Path>,
{
    std::fs::copy(source.as_ref(), dest.as_ref()).map_err(|e| FileIoError::CopyFile {
        source_path: source.as_ref().into(),
        dest_path: dest.as_ref().into(),
        err: e,
    })?;
    Ok(())
}

pub fn open_file<P: AsRef<Path>>(path: P) -> Result<File, FileIoError> {
    File::open(path.as_ref()).map_err(|e| FileIoError::OpenFile {
        path: path.as_ref().into(),
        err: e,
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    use tempfile::tempdir;

    #[test]
    fn create_dir__can_create_dir_in_valid_path() {
        // Given
        let tempdir = tempdir().unwrap();
        let workdir = tempdir.path();
        let new_dir = workdir.join("mynewdir");

        // When
        create_dir(new_dir.clone(), true).unwrap();

        // Then
        assert!(new_dir.exists());
        assert!(new_dir.is_dir());
    }

    #[test]
    fn create_dir__nonexistent_parent_dir_returns_error_with_path() {
        // Given
        let tempdir = tempdir().unwrap();
        let workdir = tempdir.path();
        let new_dir = workdir.join("thisdirdoesntexist/creationwillfail");

        // When
        let res = create_dir(new_dir.clone(), true).unwrap_err();

        // Then
        let expected_path = new_dir;
        assert_matches!(
            res,
            FileIoError::CreateDir {
                path,
                ..
            } if path == expected_path
        );
    }
}
