use super::*;
use crate::app::{
    adapters::ip_provider::docker_ip_provider::DockerIpProvider, key_provider::ValidatorKeySet,
};
use compose_yml::v2::{value, CommandLine, RawOr, VolumeMount};
use insta::assert_yaml_snapshot;

impl DockerComposeValidator {
    #[cfg(test)]
    pub fn test() -> Self {
        Self {
            validator: Validator::test(),
            index: 0,
            version: ImageVersion(
                "gcr.io/xand-dev/validator:70d328334fb490ada2e43cb5ce1d76e68ff349ac".into(),
            ),
            bootnode_url_vec: vec!["/dns4/validator-0/tcp/30333/p2p/12D3KooWJGztb1B1LYmE6qHJseWJexNz5bz2eZ1mV3NeZqZRQ3Ck","/dns4/validator-1/tcp/30333/p2p/12D3KooWJJnCu2nqHkFXR8GjiabYSiXZd8Qfpy4FbWoXaKEoV9XZ"].into(),
            chain_spec: ChainSpec::test()
        }
    }
}

#[test]
fn get_service_name__incorporates_index_in_service_name() {
    // Given
    let mut dc_validator = DockerComposeValidator::test();
    dc_validator.index = 1;

    // When
    let name = dc_validator.get_service_name();

    // Then
    assert_eq!(name, "validator-1");
}

#[test]
fn build_service__volume_paths_incorporates_index() {
    // Given
    let mut dc_validator = DockerComposeValidator::test();
    dc_validator.index = 1;

    // When
    let service = dc_validator.build_validator_service("./validators/1".into());

    // Then
    let actual_volume = service.volumes;
    let expected_volumes: Vec<RawOr<VolumeMount>> = vec![
        (
            "./validators/1/xand/chainspec/chainSpecModified.json",
            "/xand/chainspec/chainSpecModified.json",
        ),
        ("./validators/1/xand/validator", "/xand/validator/"),
        ("./validators/1/xand/keypairs", "/xand/keypairs"),
        ("./validators/1/xand-api/initial-keys", "/etc/xand-keys"),
        ("./validators/1/xand-api/key-management", "/xand/keys"),
        (
            "./validators/1/./xand-api/jwt-secret.txt",
            "/etc/xand/jwt-secret.txt",
        ),
    ]
    .into_iter()
    .map(|(host, container)| VolumeMount::host(host, container))
    .map(value)
    .collect();

    assert_eq!(actual_volume, expected_volumes);
}

#[test]
fn build_service__command_matches_validator_index() {
    // Given
    let mut dc_validator = DockerComposeValidator::test();
    dc_validator.index = 4;

    // When
    let service = dc_validator.build_validator_service("./".into());

    // Then
    let expected_command = vec![
        "--base-path",
        "/xand/validator",
        "--no-mdns",
        "--unsafe-ws-external",
        "--unsafe-rpc-external",
        "--rpc-cors",
        "all",
        "--prometheus-external",
        "--execution",
        "NativeElseWasm",
        "--public-addr",
        "/dns4/validator-4",
        "--jwt-secret-path",
        "/etc/xand/jwt-secret.txt",
    ]
    .into_iter()
    .map(ToString::to_string)
    .map(value)
    .collect();
    let expected_command = CommandLine::Parsed(expected_command);
    assert_eq!(service.command, Some(expected_command))
}

#[test]
fn build_service__serialized_service_matches_expected() {
    // Given
    let mut dc_validator = DockerComposeValidator::test();
    dc_validator.version =
        ImageVersion("gcr.io/xand-dev/validator:70d328334fb490ada2e43cb5ce1d76e68ff349ac".into());
    dc_validator.index = 0;
    let node_key = dc_validator.validator.state().borrow().node_key();
    // When
    let service = dc_validator.build_validator_service("./validators/0/".into());

    // Then
    let expected_yaml = r#"---
image: gcr.io/xand-dev/validator:70d328334fb490ada2e43cb5ce1d76e68ff349ac
command: ["--base-path", "/xand/validator", "--no-mdns", "--unsafe-ws-external",
      "--unsafe-rpc-external", "--rpc-cors", "all", "--prometheus-external",
      "--execution", "NativeElseWasm", "--public-addr", "/dns4/validator-0",
      "--jwt-secret-path", "/etc/xand/jwt-secret.txt"]
volumes:
- ./validators/0/xand/chainspec/chainSpecModified.json:/xand/chainspec/chainSpecModified.json
- ./validators/0/xand/validator:/xand/validator/
- ./validators/0/xand/keypairs:/xand/keypairs
- ./validators/0/xand-api/initial-keys:/etc/xand-keys
- ./validators/0/xand-api/key-management:/xand/keys
- ./validators/0/./xand-api/jwt-secret.txt:/etc/xand/jwt-secret.txt
env_file:
- ./validators/0/xand/validator-lp2p-key.env
environment:
  NODE_KEY: "MODIFY ME I'M GENERATED!"
  CONSENSUS_NODE: 'true'
  BOOTNODE_URL_VEC: /dns4/validator-0/tcp/30333/p2p/12D3KooWJGztb1B1LYmE6qHJseWJexNz5bz2eZ1mV3NeZqZRQ3Ck /dns4/validator-1/tcp/30333/p2p/12D3KooWJJnCu2nqHkFXR8GjiabYSiXZd8Qfpy4FbWoXaKEoV9XZ
  NODE_NAME: Xand0
  TELEMETRY_URL: ws://substrate-telemetry:1024 0
  EMIT_METRICS: 'true'
  XAND_HUMAN_LOGGING: 'true'
ports:
- "10042:10044"
expose:
- "8933"
- "9944"
- "30333"
- "10044"
networks:
  xand: {}"#;
    let mut expected_service: Service = serde_yaml::from_str(expected_yaml).unwrap();
    expected_service
        .environment
        .insert("NODE_KEY".into(), value(node_key));
    assert_eq!(service, expected_service);
}

#[test]
fn build_service__when_validator_disables_jwt_then_serialized_service_excludes_jwt_flags_and_volume(
) {
    // Given
    let root_dir = XandNodeDirConventionBuilder("./some/root/dir".into());
    let walleted_validator = Validator::new(ValidatorKeySet::test());
    let deployable_validator = walleted_validator
        .transition(vec![DockerIpProvider::test().get_subnet().addr()])
        .transition(vec![], None);
    let mut dcv = DockerComposeValidator::test();
    dcv.validator = deployable_validator;

    // When
    let service = dcv.build_validator_service(root_dir);

    // Then
    assert_yaml_snapshot!(service);
}
