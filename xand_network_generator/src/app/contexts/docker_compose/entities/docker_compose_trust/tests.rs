use crate::{
    app::{
        adapters::{
            auth_provider::AuthProvider,
            bank_info_provider::docker_bank_info_provider::DockerBankInfoProvider,
            ip_provider::docker_ip_provider::DockerIpProvider,
        },
        contexts::docker_compose::{
            docker_compose_network::structured_docker_compose_root::StructuredDockerComposeConv,
            entities::docker_compose_trust::DockerComposeTrust,
        },
        data::ImageVersion,
        entities::trust::Trust,
        external_wrappers::chain_spec_generation::ChainSpec,
        key_provider::TrustKeySet,
        xand_services::dir_structure::XandNodeDirConventionBuilder,
    },
    contracts::data::bank_info::{Account, Bank, BankInfo},
};
use compose_yml::v2::Service;
use insta::assert_yaml_snapshot;
use std::collections::HashSet;
use strum::IntoEnumIterator;

impl DockerComposeTrust {
    #[cfg(test)]
    pub fn test() -> Self {
        Self::test_with_banks(&Bank::iter().collect())
    }

    #[cfg(test)]
    pub fn test_with_banks(enabled_banks: &HashSet<Bank>) -> Self {
        let auth = Some(AuthProvider::get_test_xand_api_jwt_info());
        Self {
          trust: Trust::new(&DockerBankInfoProvider::new(), &mut DockerIpProvider::test(), TrustKeySet::test(), enabled_banks, auth),
          version: ImageVersion("gcr.io/xand-dev/trust:1.1.1".into()),
          validator_version: ImageVersion("gcr.io/xand-dev/validator:70d328334fb490ada2e43cb5ce1d76e68ff349ac"
              .into()),
            trust_api_version: ImageVersion("gcr.io/xand-dev/trust-api:1.1.1".into()),
            depends_on_services: vec![],
          bootnode_url_vec: vec!["/dns4/validator-0/tcp/30333/p2p/12D3KooWJGztb1B1LYmE6qHJseWJexNz5bz2eZ1mV3NeZqZRQ3Ck"].into(),

          chain_spec: ChainSpec::test(),
      }
    }
}

#[test]
fn build_nc_node_service__service_matches_expected() {
    // Given
    let mut dc_member = DockerComposeTrust::test();
    let test_service = "testdependentservice";
    dc_member.depends_on_services = vec![test_service.into()];
    dc_member.bootnode_url_vec = vec!["/dns4/validator-0/tcp/30333/p2p/12D3KooWJGztb1B1LYmE6qHJseWJexNz5bz2eZ1mV3NeZqZRQ3Ck /dns4/validator-1/tcp/30333/p2p/12D3KooWJJnCu2nqHkFXR8GjiabYSiXZd8Qfpy4FbWoXaKEoV9XZ"].into();
    let root_dir = StructuredDockerComposeConv {}.trust_nc_node_dir();
    let dir_convention = XandNodeDirConventionBuilder::from(root_dir);

    // When
    let service = dc_member.build_nc_node_service(&dir_convention);

    // Then
    let expected_yaml = r#"---
image: gcr.io/xand-dev/validator:70d328334fb490ada2e43cb5ce1d76e68ff349ac
command: ["--base-path", "/xand/validator", "--no-mdns", "--unsafe-ws-external",
          "--unsafe-rpc-external", "--rpc-cors", "all", "--prometheus-external",
          "--execution", "NativeElseWasm", "--public-addr", "/dns4/trust-non-consensus-node",
          "--jwt-secret-path", "/etc/xand/jwt-secret.txt"]
volumes:
  - ./trust/xand-node/xand/chainspec/chainSpecModified.json:/xand/chainspec/chainSpecModified.json
  - ./trust/xand-node/xand/validator:/xand/validator/
  - ./trust/xand-node/xand/keypairs:/xand/keypairs
  - ./trust/xand-node/xand-api/initial-keys:/etc/xand-keys
  - ./trust/xand-node/xand-api/key-management:/xand/keys
  - ./trust/xand-node/./xand-api/jwt-secret.txt:/etc/xand/jwt-secret.txt
environment:
  CONSENSUS_NODE: 'false'
  BOOTNODE_URL_VEC: /dns4/validator-0/tcp/30333/p2p/12D3KooWJGztb1B1LYmE6qHJseWJexNz5bz2eZ1mV3NeZqZRQ3Ck /dns4/validator-1/tcp/30333/p2p/12D3KooWJJnCu2nqHkFXR8GjiabYSiXZd8Qfpy4FbWoXaKEoV9XZ
  NODE_NAME: TrustNode
  TELEMETRY_URL: ws://substrate-telemetry:1024 0
  EMIT_METRICS: "true"
  XAND_HUMAN_LOGGING: "true"
ports:
  - "10043:10044"
expose:
  - "8933"
  - "9944"
  - "30333"
  - "10044"
depends_on:
  - testdependentservice
networks:
  xand: {}"#;
    let expected_service: Service = serde_yaml::from_str(expected_yaml).unwrap();
    assert_eq!(service, expected_service);
}

#[test]
fn build_nc_node_service__when_trust_disables_jwt_serialized_yaml_doesnt_include_jwt_flags_and_volume(
) {
    // Given
    let root_dir = XandNodeDirConventionBuilder("./some/root/dir".into());
    let mut trust = Trust::test();
    trust.state.xand_api_auth = None;
    let mut dct = DockerComposeTrust::test();
    dct.trust = trust;

    // When
    let service = dct.build_nc_node_service(&root_dir);

    // Then
    assert_yaml_snapshot!(service);
}

#[test]
fn build_trust_service__serialized_service_matches_expected() {
    // Given
    let dc_member = DockerComposeTrust::test();

    // When
    let service = dc_member.build_trust_service(&StructuredDockerComposeConv {});

    // Then
    let expected_yaml = r#"---
image: gcr.io/xand-dev/trust:1.1.1
entrypoint: "./trustee-node"
command: ["/etc/trustee-node/config", "--allow-create-db"]
restart: on-failure
volumes:
  - ./trust/trust-software/config:/etc/trustee-node/config
environment:
  XAND_HUMAN_LOGGING: "true"
depends_on:
  - trust-non-consensus-node
networks:
  xand: {}"#;
    let expected_service: Service = serde_yaml::from_str(expected_yaml).unwrap();
    assert_eq!(service, expected_service);
}

#[test]
fn build_trust_api_service__serialized_service_matches_expected() {
    // Given
    let dc_member = DockerComposeTrust::test();

    // When
    let service = dc_member.build_trust_api_service(&StructuredDockerComposeConv {});

    // Then
    let expected_yaml = r#"---
image: gcr.io/xand-dev/trust-api:1.1.1
restart: on-failure
volumes:
  - ./trust/trust-api/config:/etc/xand/trust-api/config
environment:
  XAND_HUMAN_LOGGING: "true"
depends_on:
  - trust-non-consensus-node
networks:
  xand: {}
ports:
  - "8080:8080""#;
    let expected_service: Service = serde_yaml::from_str(expected_yaml).unwrap();
    assert_eq!(service, expected_service);
}

#[test]
fn metadata__lists_all_banks_when_all_enabled() {
    // Given
    let trust = DockerComposeTrust::test();

    // When
    let metadata = trust.metadata();

    // Then
    assert_eq!(
        metadata.banks,
        vec![
            BankInfo {
                bank: Bank::Mcb,
                account: Account::new(121141343, 1000000),
            },
            BankInfo {
                bank: Bank::Provident,
                account: Account::new(211374020, 1000000),
            },
            BankInfo {
                bank: Bank::TestBankOne,
                account: Account::new(990000000, 1000000),
            },
            BankInfo {
                bank: Bank::TestBankTwo,
                account: Account::new(991000000, 1000000),
            }
        ]
    )
}

#[test]
fn metadata__lists_partial_banks_when_only_some_enabled() {
    // Given
    let enabled_banks = [Bank::Mcb, Bank::TestBankTwo].iter().cloned().collect();
    let trust = DockerComposeTrust::test_with_banks(&enabled_banks);

    // When
    let metadata = trust.metadata();

    // Then
    assert_eq!(
        metadata.banks,
        vec![
            BankInfo {
                bank: Bank::Mcb,
                account: Account::new(121141343, 1000000),
            },
            BankInfo {
                bank: Bank::TestBankTwo,
                account: Account::new(991000000, 1000000),
            }
        ]
    )
}
