#[cfg(test)]
mod tests;

use crate::{
    app::{
        contexts::docker_compose::{
            service_naming_convention::ServiceNamingConvention,
            services::non_consensus_node::{
                non_consensus_node_spec::NonConsensusNodeSpec, NonConsensusNode,
                LIMITED_AGENT_XAND_API_HOST_PORT,
            },
        },
        data::ImageVersion,
        entities::limited_agent::{
            states::deployable_limited_agent::DeployableLimitedAgent, LimitedAgent,
        },
        external_wrappers::chain_spec_generation::ChainSpec,
        xand_services::dir_structure::XandNodeDirConventionBuilder,
    },
    contracts::network::metadata::{
        bootnode_url_vec::BootnodeUrlVec,
        xand_entity_set_summary::{LimitedAgentMetadata, XandApiDetails},
    },
};
use compose_yml::v2::{value, Service};
use url::Url;

pub struct DockerComposeLimitedAgent {
    pub limited_agent: LimitedAgent<DeployableLimitedAgent>,
    validator_version: ImageVersion,
    depends_on_services: Vec<String>,
    bootnode_url_vec: BootnodeUrlVec,
    pub chain_spec: ChainSpec,
}

impl DockerComposeLimitedAgent {
    pub fn new(
        limited_agent: LimitedAgent<DeployableLimitedAgent>,
        validator_version: ImageVersion,
        depends_on_services: Vec<String>,
        bootnode_url_vec: BootnodeUrlVec,
        chain_spec: ChainSpec,
    ) -> Self {
        DockerComposeLimitedAgent {
            limited_agent,
            validator_version,
            depends_on_services,
            bootnode_url_vec,
            chain_spec,
        }
    }

    // build nc node service
    pub fn get_nc_node_service_name(&self) -> String {
        ServiceNamingConvention::limited_agent_non_consensus_node()
    }

    pub fn build_nc_node_service(&self, dir_convention: &XandNodeDirConventionBuilder) -> Service {
        let multi_addr_dns = format!("/dns4/{}", self.get_nc_node_service_name());
        let la_ncn_dir = dir_convention.0.clone();
        let node_name = "LimitedAgentNode";

        let maybe_jwt_secret_filepath = self
            .limited_agent
            .xand_api_auth()
            .as_ref()
            .map(|_auth| dir_convention.get_xand_api_jwt_secret_path());

        let ncn_spec = NonConsensusNodeSpec {
            validator_version: self.validator_version.clone(),
            node_name: node_name.to_string(),
            bootnode_url_vec: self.bootnode_url_vec.clone(),
            multi_addr_dns,
            rel_path_to_service_dir: la_ncn_dir,
            xand_api_host_port: LIMITED_AGENT_XAND_API_HOST_PORT,
            xand_api_jwt_secret_path: maybe_jwt_secret_filepath,
        };

        let mut s = NonConsensusNode::build(ncn_spec);

        s.depends_on = self
            .depends_on_services
            .clone()
            .into_iter()
            .map(value)
            .collect();
        s
    }

    pub fn metadata(&self) -> LimitedAgentMetadata {
        let xand_api_url = Url::parse(&format!(
            "http://localhost:{}",
            LIMITED_AGENT_XAND_API_HOST_PORT
        ))
        .unwrap();

        LimitedAgentMetadata {
            address: self.limited_agent.kp().get_address(),
            xand_api_details: XandApiDetails {
                xand_api_url,
                auth: self.limited_agent.xand_api_auth().clone(),
            },
        }
    }
}
