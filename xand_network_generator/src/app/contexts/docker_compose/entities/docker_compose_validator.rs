use crate::app::entities::validator::states::deployable_validator::DeployableValidator;
use crate::{
    app::{
        contexts::docker_compose::{
            service_builder::ServiceBuilder,
            service_naming_convention::ServiceNamingConvention,
            services::non_consensus_node::{
                STARTING_VALIDATORS_XAND_API_HOST_PORT, XAND_API_CONTAINER_PORT,
            },
        },
        data::ImageVersion,
        entities::validator::Validator,
        external_wrappers::chain_spec_generation::ChainSpec,
        xand_services::dir_structure::XandNodeDirConventionBuilder,
    },
    contracts::network::metadata::bootnode_url_vec::BootnodeUrlVec,
};
use compose_yml::v2::Service;
use std::convert::TryInto;

#[cfg(test)]
mod tests;

pub struct DockerComposeValidator {
    pub validator: Validator<DeployableValidator>,
    pub index: usize,
    version: ImageVersion,
    bootnode_url_vec: BootnodeUrlVec,
    pub chain_spec: ChainSpec,
}

impl DockerComposeValidator {
    pub fn new(
        validator: Validator<DeployableValidator>,
        index: usize,
        version: ImageVersion,
        bootnode_url_vec: BootnodeUrlVec,
        chain_spec: ChainSpec,
    ) -> Self {
        Self {
            validator,
            index,
            version,
            bootnode_url_vec,
            chain_spec,
        }
    }

    pub fn get_service_name(&self) -> String {
        ServiceNamingConvention::validator_node(self.index)
    }

    pub fn build_validator_service(
        &self,
        structured_xand_node_dir: XandNodeDirConventionBuilder,
    ) -> Service {
        let mut s = ServiceBuilder::default_xand_node();

        let multi_addr_dns = format!("/dns4/{}", self.get_service_name());

        let validator_volume_prefix = structured_xand_node_dir.root_path().clone();
        // TODO: Given a serialzied directory from StructuredValRoot, should just be able to mount it in once as is (instead of building relative paths to specific artifacts)
        let p2p_env_file = structured_xand_node_dir.get_libp2p_key_filepath();

        let node_name = format!("Xand{}", self.index);
        let node_key = self.validator.state().borrow().node_key();

        let xand_api_host_port = self.xand_api_host_port();

        s.set_image(&self.version)
            .set_public_addr(multi_addr_dns)
            // TODO: Consolidate between passing in env var files (for nodekey) and encoding vars into service definition
            //  Both seem to have references to NODE_KEY
            .add_env_file(p2p_env_file)
            .set_node_key(node_key)
            .set_consensus(true)
            .set_bootnode_url_vec(&self.bootnode_url_vec)
            .set_node_name(&node_name)
            .prefix_volumes_with(validator_volume_prefix)
            .set_port_mappings(vec![(xand_api_host_port, XAND_API_CONTAINER_PORT)]);

        // If validator has JwtInfo for XandAPI, add jwt flags and mount in jwt-secret.txt
        if let Some(_jwt_info) = self.validator.state().borrow().xand_api_auth() {
            s.add_jwt_flag_to_command_line_args();
            s.add_jwt_secret_txt_volume(structured_xand_node_dir.get_xand_api_jwt_secret_path());
        }

        s.build()
    }

    pub fn xand_api_host_port(&self) -> u16 {
        (STARTING_VALIDATORS_XAND_API_HOST_PORT - self.index)
            .try_into()
            .unwrap()
    }
}
