use super::*;
use compose_yml::v2::{value, CommandLine, PortMapping, VolumeMount};
use strum::IntoEnumIterator;

impl BankMocks {
    pub fn test() -> Self {
        Self {
            version: ImageVersion("gcr.io/xand-dev/xand-bank-mocks:blah".to_string()),
            enabled_banks: Bank::iter().collect(),
        }
    }
}

#[test]
fn build_service__builds_with_given_image() {
    // Given
    let bm = BankMocks {
        version: ImageVersion("gcr.io/xand-dev/xand-bank-mocks:bobloblaw".to_string()),
        enabled_banks: Bank::iter().collect(),
    };

    // When
    let svc = bm.build_service("./".into());

    // Then
    assert_eq!(
        svc.image.unwrap().to_string(),
        "gcr.io/xand-dev/xand-bank-mocks:bobloblaw"
    );
}

#[test]
fn build_service__has_entrypoint() {
    // Given
    let bm = BankMocks::test();

    // When
    let svc = bm.build_service("./".into());

    // Then
    let command = svc.entrypoint.unwrap();
    assert_eq!(command, CommandLine::ShellCode(value("yarn".to_string())));
}

#[test]
fn build_service__has_command_args_vec() {
    // Given
    let bm = BankMocks::test();

    // When
    let svc = bm.build_service("./".into());

    // Then
    let cmd_args = svc.command.unwrap();
    let expected_args = vec!["start", "8888"]
        .into_iter()
        .map(|w| value(w.to_string()))
        .collect(); //value("yarn".to_string());
    assert_eq!(cmd_args, CommandLine::Parsed(expected_args));
}

#[test]
fn build_service__has_mapped_port() {
    // Given
    let bm = BankMocks::test();

    // When
    let svc = bm.build_service("./".into());

    // Then
    let mapping = svc.ports.get(0).unwrap().value().unwrap();
    let p = &PortMapping::new(8888, 8888);
    assert_eq!(mapping, p);
}

#[test]
fn build_service__has_volumes() {
    // Given
    let bm = BankMocks::test();

    // When
    let svc = bm.build_service("./bank-mocks".into());

    // Then
    let volume = svc.volumes.get(0).unwrap().value().unwrap();
    let expected = &VolumeMount::host("./bank-mocks/./", "/etc/xand/bank-mocks/");
    assert_eq!(volume, expected);
}

#[test]
fn build_service__is_in_xand_network() {
    // Given
    let bm = BankMocks::test();

    // When
    let svc = bm.build_service("./".into());

    // Then
    let expected = "xand";
    let found: Vec<String> = svc.networks.keys().map(String::to_string).collect();
    assert!(
        svc.networks.contains_key(expected),
        "Expected: {}\tFound: {:?}",
        expected,
        found,
    );
}

#[test]
fn serialize_config_to__() {
    // Given
    let bm = BankMocks::test();

    // When
    let svc = bm.build_service("./".into());

    // Then
    let expected = "xand";
    let found: Vec<String> = svc.networks.keys().map(String::to_string).collect();
    assert!(
        svc.networks.contains_key(expected),
        "Expected: {}\tFound: {:?}",
        expected,
        found,
    );
}
