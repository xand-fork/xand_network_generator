use crate::app::external_wrappers::chain_spec_generation::ChainSpec;
use crate::app::{
    contexts::docker_compose::{
        docker_compose_network::docker_xand_entity_set::DockerXandEntitySet,
        entities::{
            docker_compose_limited_agent::DockerComposeLimitedAgent,
            docker_compose_member::DockerComposeMember, docker_compose_trust::DockerComposeTrust,
            docker_compose_validator::DockerComposeValidator,
        },
        services::bank_mocks::BankMocks,
    },
    data::ImageSet,
    network::XandEntitySet,
};
use crate::contracts::network::metadata::bootnode_url_vec::BootnodeUrlVec;

pub struct DockerXandEntitySetBuilder {
    entity_set: XandEntitySet,
    image_version_set: ImageSet,
    bootnode_url_vec: BootnodeUrlVec,
    chain_spec: ChainSpec,
}

impl DockerXandEntitySetBuilder {
    pub fn new(
        entity_set: XandEntitySet,
        image_version_set: ImageSet,
        bootnode_url_vec: BootnodeUrlVec,
        chain_spec: ChainSpec,
    ) -> Self {
        DockerXandEntitySetBuilder {
            entity_set,
            image_version_set,
            bootnode_url_vec,
            chain_spec,
        }
    }

    pub fn build(&self) -> DockerXandEntitySet {
        let image_version_set = &self.image_version_set;
        let entity_set = &self.entity_set;
        let bootnode_url_vec = &self.bootnode_url_vec;
        let chain_spec = &self.chain_spec;

        let validator_version = image_version_set.validator.clone();
        let trust_version = image_version_set.trustee.clone();
        let trust_api_version = image_version_set.trust_api.clone();

        let banks = BankMocks::new(image_version_set.bank_mocks.clone(), entity_set.banks());

        let (on_chain_validators, off_chain_validators) = self.build_validators();

        // Build list of all validators' service names for members to depend on
        let validator_services: Vec<String> = on_chain_validators
            .iter()
            .chain(off_chain_validators.iter())
            .map(|v| v.get_service_name())
            .collect();

        // Set up Members' indices and partition by permission
        let (on_chain_members, off_chain_members) = self.build_members(validator_services.clone());

        // Build Trust
        let trust = DockerComposeTrust::new(
            entity_set.trust(),
            trust_version,
            trust_api_version,
            validator_version.clone(),
            validator_services.clone(),
            bootnode_url_vec.clone(),
            chain_spec.clone(),
        );

        let limited_agent = DockerComposeLimitedAgent::new(
            entity_set.limited_agent(),
            validator_version,
            validator_services,
            bootnode_url_vec.clone(),
            chain_spec.clone(),
        );

        DockerXandEntitySet {
            banks,
            trust,
            limited_agent,
            permissioned_validators: on_chain_validators,
            extra_validators: off_chain_validators,
            permissioned_members: on_chain_members,
            extra_members: off_chain_members,
        }
    }

    fn build_members(
        &self,
        member_depends_on: Vec<String>,
    ) -> (Vec<DockerComposeMember>, Vec<DockerComposeMember>) {
        let on_chain_members = self.entity_set.members();
        let on_chain_members_len = on_chain_members.len();
        let off_chain_members = self.entity_set.extra_members();

        let mut all_members: Vec<DockerComposeMember> = on_chain_members
            .into_iter()
            .chain(off_chain_members.into_iter())
            .enumerate()
            .map(|(idx, m)| {
                DockerComposeMember::new(
                    m,
                    idx,
                    self.image_version_set.member_api.clone(),
                    self.image_version_set.validator.clone(),
                    member_depends_on.clone(),
                    self.bootnode_url_vec.clone(),
                    self.chain_spec.clone(),
                )
            })
            .collect();
        let off_chain_members: Vec<DockerComposeMember> =
            all_members.drain(on_chain_members_len..).collect();
        let on_chain_members = all_members;
        (on_chain_members, off_chain_members)
    }

    fn build_validators(&self) -> (Vec<DockerComposeValidator>, Vec<DockerComposeValidator>) {
        // Chain together to get incrementing index value across all validators
        let on_chain_validators = self.entity_set.validators();
        let on_chain_validators_len = on_chain_validators.len();

        let off_chain_validators = self.entity_set.extra_validators();

        let mut all_validators: Vec<DockerComposeValidator> = on_chain_validators
            .into_iter()
            .chain(off_chain_validators.into_iter())
            .enumerate()
            .map(|(idx, v)| {
                DockerComposeValidator::new(
                    v,
                    idx,
                    self.image_version_set.validator.clone(),
                    self.bootnode_url_vec.clone(),
                    self.chain_spec.clone(),
                )
            })
            .collect();

        // Split off non_permissioned validators
        let off_chain_validators = all_validators.drain(on_chain_validators_len..).collect();
        let on_chain_validators = all_validators;
        (on_chain_validators, off_chain_validators)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::app::network::{GenerateMembersSpec, GenerateValidatorsSpec};
    use crate::contracts::data::bank_info::Bank;
    use strum::IntoEnumIterator;

    impl DockerXandEntitySetBuilder {
        pub fn test() -> Self {
            Self {
                entity_set: XandEntitySet::test(),
                image_version_set: ImageSet::test(),
                bootnode_url_vec: BootnodeUrlVec::test(),
                chain_spec: ChainSpec::test(),
            }
        }
    }

    #[test]
    fn build__number_of_members_and_vals_match_given_entity_set() {
        // Given
        let entity_set = XandEntitySet::test();
        let image_set = ImageSet::test();
        let bootnode_url_vec = BootnodeUrlVec::test();
        let chain_spec = ChainSpec::test();
        let builder = DockerXandEntitySetBuilder::new(
            entity_set.clone(),
            image_set,
            bootnode_url_vec,
            chain_spec,
        );

        // When
        let des = builder.build();

        // Then
        assert_eq!(des.permissioned_members.len(), entity_set.members().len());
        assert_eq!(des.extra_members.len(), entity_set.extra_members().len());

        assert_eq!(
            des.permissioned_validators.len(),
            entity_set.validators().len()
        );
        assert_eq!(
            des.extra_validators.len(),
            entity_set.extra_validators().len()
        )
    }

    #[test]
    fn build__a_member_nc_node_depends_on_all_validators() {
        // Given
        let entity_set = XandEntitySet::test();
        let image_set = ImageSet::test();
        let bootnode_url_vec = BootnodeUrlVec::test();
        let chain_spec = ChainSpec::test();
        let builder =
            DockerXandEntitySetBuilder::new(entity_set, image_set, bootnode_url_vec, chain_spec);

        // When
        let des = builder.build();

        // Then
        let val_service_names: Vec<String> = des
            .permissioned_validators
            .iter()
            .chain(des.extra_validators.iter())
            .map(|v| v.get_service_name())
            .collect();

        let m_0: Vec<String> = des
            .permissioned_members
            .get(0)
            .unwrap()
            .build_nc_node_service("./".into())
            .depends_on
            .iter()
            .map(|s| s.to_string())
            .collect();

        assert_eq!(m_0, val_service_names);
    }

    #[test]
    fn build__trust_nc_node_depends_on_all_validators() {
        // Given
        let entity_set = XandEntitySet::test();
        let image_set = ImageSet::test();
        let bootnode_url_vec = BootnodeUrlVec::test();
        let chain_spec = ChainSpec::test();
        let builder =
            DockerXandEntitySetBuilder::new(entity_set, image_set, bootnode_url_vec, chain_spec);

        // When
        let des = builder.build();

        // Then
        let val_service_names: Vec<String> = des
            .permissioned_validators
            .iter()
            .chain(des.extra_validators.iter())
            .map(|v| v.get_service_name())
            .collect();
        let trust_ncn_depends_on: Vec<String> = des
            .trust
            .build_nc_node_service(&"./".into())
            .depends_on
            .iter()
            .map(|s| s.to_string())
            .collect();
        assert_eq!(trust_ncn_depends_on, val_service_names);
    }

    #[test]
    fn build_validators__indexes_all_validators_together() {
        // Given
        let spec = GenerateValidatorsSpec::new(2, 3);
        let builder = get_builder_with_validators(spec);

        // When
        let (on_chain_vals, off_chain_vals) = builder.build_validators();

        // Then
        let indices: Vec<usize> = on_chain_vals
            .iter()
            .chain(off_chain_vals.iter())
            .map(|v| v.index)
            .collect();
        assert_eq!(indices, vec![0, 1, 2, 3, 4]);
    }

    #[test]
    fn build_validators__returns_expected_count_of_validators_by_permission() {
        // Given
        let spec = GenerateValidatorsSpec::new(2, 3);
        let builder = get_builder_with_validators(spec);

        // When
        let (on_chain_vals, off_chain_vals) = builder.build_validators();

        // Then
        assert_eq!(on_chain_vals.len(), 2);
        assert_eq!(off_chain_vals.len(), 3);
    }

    #[test]
    fn build_members__indexes_all_members_together() {
        // Given
        let spec = GenerateMembersSpec::new(2, 3);
        let builder = get_builder_with_members(spec);
        let members_depend_on = vec![];
        // When
        let (on_chain_mems, off_chain_mems) = builder.build_members(members_depend_on);

        // Then
        let indices: Vec<usize> = on_chain_mems
            .iter()
            .chain(off_chain_mems.iter())
            .map(|v| v.index)
            .collect();
        assert_eq!(indices, vec![0, 1, 2, 3, 4]);
    }

    #[test]
    fn build_members__returns_expected_count_of_members_by_permission() {
        // Given
        let spec = GenerateMembersSpec::new(2, 3);
        let builder = get_builder_with_members(spec);
        let members_depend_on = vec![];
        // When
        let (on_chain_mems, off_chain_mems) = builder.build_members(members_depend_on);

        // Then
        assert_eq!(on_chain_mems.len(), 2);
        assert_eq!(off_chain_mems.len(), 3);
    }

    #[test]
    fn build_members__depends_on_given_services() {
        // Given
        let spec = GenerateMembersSpec::new(2, 3);
        let builder = get_builder_with_members(spec);
        let members_depend_on: Vec<String> = vec!["service-1".into(), "service-2".into()];

        // When
        let (on_chain_mems, off_chain_mems) = builder.build_members(members_depend_on.clone());

        // Then
        assert!(on_chain_mems
            .iter()
            .all(|m| m.depends_on_services == members_depend_on));
        assert!(off_chain_mems
            .iter()
            .all(|m| m.depends_on_services == members_depend_on));
    }

    fn get_builder_with_validators(spec: GenerateValidatorsSpec) -> DockerXandEntitySetBuilder {
        let mut builder = DockerXandEntitySetBuilder::test();

        let entity_set = XandEntitySet::generate(
            GenerateMembersSpec::test(),
            spec,
            Some(0),
            Bank::iter().collect(),
            false,
        )
        .unwrap();
        builder.entity_set = entity_set;
        builder
    }

    fn get_builder_with_members(spec: GenerateMembersSpec) -> DockerXandEntitySetBuilder {
        let mut builder = DockerXandEntitySetBuilder::test();

        let entity_set = XandEntitySet::generate(
            spec,
            GenerateValidatorsSpec::test(),
            Some(0),
            Bank::iter().collect(),
            false,
        )
        .unwrap();
        builder.entity_set = entity_set;
        builder
    }
}
