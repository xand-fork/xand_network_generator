use crate::app::contexts::docker_compose::{
    docker_compose_network::*,
    entities::{
        docker_compose_member::DockerComposeMember,
        docker_compose_validator::DockerComposeValidator,
    },
    service_naming_convention::ServiceNamingConvention,
};

#[test]
fn build_docker_compose_yaml__network_is_xand() {
    // Given
    let dcc = DockerComposeNetwork::test();

    // when
    let dcyaml: File = dcc.build_docker_compose_yaml();

    // Then
    assert!(dcyaml.networks.contains_key("xand"));
}

#[test]
fn build_docker_compose_yaml__has_validators() {
    // Given
    let dcc = DockerComposeNetwork::test();

    // when
    let dcyaml: File = dcc.build_docker_compose_yaml();

    // Then
    let svc = ServiceNamingConvention::validator_node(0);
    assert!(dcyaml.services.contains_key(&svc));
}

#[test]
fn build_docker_compose_yaml__has_members() {
    // Given
    let dcc = DockerComposeNetwork::test();

    // when
    let dcyaml: File = dcc.build_docker_compose_yaml();

    // Then
    let api = ServiceNamingConvention::member(0);
    let nc_node = ServiceNamingConvention::non_consensus_node("member", 0);
    assert!(dcyaml.services.contains_key(&api));
    assert!(dcyaml.services.contains_key(&nc_node));
}

#[test]
fn build_docker_compose_yaml__has_trust() {
    // Given
    let dcc = DockerComposeNetwork::test();

    // when
    let dcyaml: File = dcc.build_docker_compose_yaml();

    // Then
    let svc = ServiceNamingConvention::trustee();
    assert!(dcyaml.services.contains_key(&svc));
}

#[test]
fn build_docker_compose_yaml__has_bank_mocks() {
    // Given
    let dcc = DockerComposeNetwork::test();

    // when
    let dcyaml: File = dcc.build_docker_compose_yaml();

    // Then
    let svc = ServiceNamingConvention::bank_mocks();
    assert!(dcyaml.services.contains_key(&svc));
}

#[test]
fn build_docker_compose_yaml__includes_all_members() {
    // Given
    let mut docker_xand_entity_set = DockerXandEntitySet::test();
    let mut mem_0 = DockerComposeMember::test();
    let mut mem_1 = DockerComposeMember::test();
    mem_0.index = 0;
    mem_1.index = 1;

    // Explicitly set on and off-chain members
    docker_xand_entity_set.permissioned_members = vec![mem_0];
    docker_xand_entity_set.extra_members = vec![mem_1];

    let dcn = DockerComposeNetwork {
        docker_xand_entity_set,
    };

    // when
    let dcyaml: File = dcn.build_docker_compose_yaml();

    // Then
    assert!(dcyaml.services.contains_key("member-api-0"));
    assert!(dcyaml.services.contains_key("member-non-consensus-node-0"));
    assert!(dcyaml.services.contains_key("member-api-1"));
    assert!(dcyaml.services.contains_key("member-non-consensus-node-1"));
}

#[test]
fn build_docker_compose_yaml__includes_all_validators() {
    // Given
    let mut docker_xand_entity_set = DockerXandEntitySet::test();
    let mut val_0 = DockerComposeValidator::test();
    let mut val_1 = DockerComposeValidator::test();
    val_0.index = 0;
    val_1.index = 1;

    // Explicitly set on and off-chain members
    docker_xand_entity_set.permissioned_validators = vec![val_0];
    docker_xand_entity_set.extra_validators = vec![val_1];

    let dcn = DockerComposeNetwork {
        docker_xand_entity_set,
    };

    // when
    let dcyaml: File = dcn.build_docker_compose_yaml();

    // Then
    assert!(dcyaml.services.contains_key("validator-0"));
    assert!(dcyaml.services.contains_key("validator-1"));
}
