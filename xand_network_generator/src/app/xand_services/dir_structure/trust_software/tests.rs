use crate::app::contexts::docker_compose::services::bank_url_provider::tests::DummyBankUrlProvider;

use super::*;
use tempfile::tempdir;
use trustee_node_config::TrusteeConfig;

#[test]
fn write_trust__directory_structure_matches_expected() {
    // Given
    let dcm = DockerComposeTrust::test();
    let tempdir = tempdir().unwrap();
    let builder = TrustDirectoryBuilder::from(tempdir.path().to_path_buf());
    let validator_accounts = Default::default();
    let member_accounts = Default::default();
    let bank_urls = DummyBankUrlProvider;

    // When
    builder
        .write_trust(&dcm, &validator_accounts, &member_accounts, &bank_urls)
        .unwrap();

    // Then
    let config_dir = tempdir.path().join("config");
    let secrets_dir = config_dir.join("secrets");

    assert!(config_dir.is_dir());
    assert!(secrets_dir.is_dir());
    assert!(config_dir.join("config.yaml").is_file());
    assert!(secrets_dir.join("secrets.yaml").is_file());
}

#[test]
fn write_trust__writes_parseable_config_yaml_file() {
    // Given
    let dcm = DockerComposeTrust::test();
    let tempdir = tempdir().unwrap();
    let builder = TrustDirectoryBuilder::from(tempdir.path().to_path_buf());
    let member_accounts = Default::default();
    let validator_accounts = Default::default();
    let bank_urls = DummyBankUrlProvider;

    // When
    let written_dir_convention = builder
        .write_trust(&dcm, &member_accounts, &validator_accounts, &bank_urls)
        .unwrap();

    // Then
    let config_file = file_io::open_file(written_dir_convention.config_file()).unwrap();
    serde_yaml::from_reader::<_, TrusteeConfig>(config_file).unwrap();
}

#[test]
fn write_trust__writes_parseable_secrets_yaml_file() {
    // Given
    let dcm = DockerComposeTrust::test();
    let tempdir = tempdir().unwrap();
    let builder = TrustDirectoryBuilder::from(tempdir.path().to_path_buf());
    let member_accounts = Default::default();
    let validator_accounts = Default::default();
    let bank_urls = DummyBankUrlProvider;

    // When
    let written_dir = builder
        .write_trust(&dcm, &member_accounts, &validator_accounts, &bank_urls)
        .unwrap();

    // Then
    let secrets_file = file_io::open_file(written_dir.secrets_file()).unwrap();
    let _secrets: HashMap<String, String> = serde_yaml::from_reader(secrets_file).unwrap();
}
