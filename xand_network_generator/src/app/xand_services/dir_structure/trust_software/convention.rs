use std::path::PathBuf;

pub struct TrustSoftwareDirConvention(pub(super) PathBuf);

impl TrustSoftwareDirConvention {
    pub fn config_dir(&self) -> PathBuf {
        self.0.join("config")
    }

    pub fn secrets_dir(&self) -> PathBuf {
        self.config_dir().join("secrets")
    }

    pub fn config_file(&self) -> PathBuf {
        self.config_dir().join("config.yaml")
    }

    pub fn secrets_file(&self) -> PathBuf {
        self.secrets_dir().join("secrets.yaml")
    }
}
