use crate::{
    app::{
        contexts::docker_compose::{
            entities::docker_compose_trust::DockerComposeTrust,
            services::{
                bank_url_provider::BankUrlProvider, non_consensus_node::XAND_API_CONTAINER_PORT,
            },
        },
        file_io,
        file_io::create_dir,
        xand_services::dir_structure::trust_software::convention::TrustSoftwareDirConvention,
    },
    contracts::data::bank_info::BankInfo,
};
use std::{collections::HashMap, path::PathBuf};

mod convention;
#[cfg(test)]
mod tests;

pub struct TrustDirectoryBuilder {
    convention: TrustSoftwareDirConvention,
}

impl TrustDirectoryBuilder {
    pub fn write_trust(
        self,
        dct: &DockerComposeTrust,
        member_accounts: &HashMap<String, Vec<BankInfo>>,
        validator_accounts: &HashMap<String, Vec<BankInfo>>,
        bank_urls: &dyn BankUrlProvider,
    ) -> crate::Result<TrustSoftwareDirConvention> {
        self.init_dirs()?;

        // Write config file to location by convention
        let xand_api_url = format!(
            "http://{}:{}",
            dct.get_nc_node_service_name(),
            XAND_API_CONTAINER_PORT
        );

        let config =
            dct.trust
                .build_config(xand_api_url, member_accounts, validator_accounts, bank_urls);
        let config_file_str = serde_yaml::to_string(&config)?;
        file_io::write_file(self.convention.config_file(), config_file_str)?;

        let secrets = dct.secrets_store();
        let secrets_str = serde_yaml::to_string(&secrets)?;
        file_io::write_file(self.convention.secrets_file(), secrets_str)?;

        Ok(self.convention)
    }

    /// Initialize for an isolated trust directory
    fn init_dirs(&self) -> crate::Result<()> {
        let config_dir = self.convention.config_dir();
        let secrets_dir = self.convention.secrets_dir();
        create_dir(&config_dir, true)?;
        create_dir(&secrets_dir, true)?;

        Ok(())
    }
}

impl From<PathBuf> for TrustDirectoryBuilder {
    fn from(p: PathBuf) -> Self {
        Self {
            convention: TrustSoftwareDirConvention(p),
        }
    }
}
