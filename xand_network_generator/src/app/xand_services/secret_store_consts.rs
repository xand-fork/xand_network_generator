/// Key for finding secret within secret store
pub const MEMBER_API_LOOKUP_KEY_JWTSECRET: &str = "member-api-jwt-secret";

/// Key for finding secret within secret store
pub const XAND_API_LOOKUP_KEY_JWTTOKEN: &str = "xand-api-jwt-token";
