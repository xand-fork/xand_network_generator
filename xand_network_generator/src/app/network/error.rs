use crate::app::adapters::auth_provider::error::AuthProviderError;
use thiserror::Error;

#[derive(Clone, Debug, Error, PartialEq, Eq)]
pub enum XandEntitySetError {
    #[error("{0}")]
    AuthProvider(#[from] AuthProviderError),
}
